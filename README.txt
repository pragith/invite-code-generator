README

Name: Invite Code Generator

Version: 1.0


Features:

--Generates a random code of 6 alpha-numeric characters with one time use
--Highlights which code has been used & not used
--Displays appropriate message in case of incorrect/correct/already used code



Install Notes:

1) Configure database connections in config.php
2) Dump your database with SQL commands provided in sql.sql file
3) Go to /admin.php to generate codes


Contact:

@PragithP
Pragith /at/ Pragith /dot/ net

If you use it, then please consider mentioning my name somewhere with a link to my website pragith.net
Thanks for using Invite Code Generator